﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

using PlayFab;
using PlayFab.ClientModels;

public class GameManager : MonoBehaviour
{
    private RewardedAd rewardedAd;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("Start()");
        //MobileAds.Initialize(initCompleteAction => { Debug.Log("MobileAds.Initialize Complete"); });
        //rewardedAd = new RewardedAd("ca-app-pub-3940256099942544/5224354917");
        //// Create an empty ad request.
        //AdRequest request = new AdRequest.Builder().Build();
        //// Load the rewarded ad with the request.
        //this.rewardedAd.LoadAd(request);

        //// Called when an ad request has successfully loaded.
        //this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        //// Called when an ad request failed to load.
        //this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        //// Called when an ad is shown.
        //this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        //// Called when an ad request failed to show.
        //this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        //// Called when the user should be rewarded for interacting with the ad.
        //this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        //// Called when the ad is closed.
        //this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;



        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        // enables saving game progress.
        //.EnableSavedGames()
        // requests the email address of the player be available.
        // Will bring up a prompt for consent.
        //.RequestEmail()
        //// requests a server auth code be generated so it can be passed to an
        ////  associated back end server application and exchanged for an OAuth token.
        //.RequestServerAuthCode(false)
        //// requests an ID token be generated.  This OAuth token can be used to
        ////  identify the player to other services such as Firebase.
        //.RequestIdToken()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();




        string playerData = "{ \"pdata\":{ \"Save\":\"{\"version\":3,\"playfabAccountId\":null,\"acceptDate\":\"Thu Dec 09 2021\",\"debugPlayTime\":0,\"timeMs\":1646650810031,\"accountName\":\"11\",\"accountLevel\":11,\"accountLevelRewardRecieved\":[\"FD1\"],\"accountExp\":921,\"stage\":2,\"selectedStage\":1,\"gold\":55083,\"diamond\":5262,\"ruby\":151,\"spiritStoneF\":0,\"spiritStoneW\":0,\"spiritStoneN\":370,\"spiritStoneS\":0,\"spiritStoneL\":0,\"manaStone\":348,\"growth\":[4,0,0,0,0,0,0,0],\"grade\":0,\"spellSlotLockedIndex\":1,\"partySlot\":[-1,-1,-1,-1],\"partyLevel\":[0,0,0,0,0,0,0,0,0,0],\"dungeonProgress\":[0],\"packages\":[0,0,0],\"adsCount\":[0,0,0,0,0],\"adsCoolTime\":[0,0,0,0,0],\"adBuffDropRemainingTime\":0,\"adBuffGoldRemainingTime\":1649.8500329999952,\"isAutoSpell\":true,\"questMissionIndex\":0,\"questMissionProgressCount\":0,\"quest-daily-ALLCLEAR\":0,\"quest-daily-GETEQUIP\":1,\"quest-daily-GETSPELL\":1,\"quest-daily-ADS\":2,\"quest-daily-TRYDUNGEON\":0,\"quest-daily-PLAYTIME\":3600,\"quest-repeat-STAGE\":1,\"quest-repeat-PLAYTIME\":5018,\"quest-repeat-MONSTER\":1172,\"quest-repeat-GETEQUIP\":5,\"quest-repeat-GETSPELL\":4,\"quest-repeat-LEVELEQUIP\":0,\"quest-repeat-POWERUP1\":3,\"quest-achievement-LEVELUP\":10,\"quest-achievement-LEVELSPELL\":0,\"quest-achievement-UPGRADEEQUIP\":0,\"quest-achievement-RECRUITPARTY\":0,\"quest-achievement-LEVELPARTY\":0,\"spellSlotCodes\":[[\"FD1\",-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1]],\"spellSlotSelected\":0,\"spellInventory\":[{\"code\":\"FD1\",\"level\":1,\"card\":0},{\"code\":\"WD1\",\"level\":1,\"card\":0},{\"code\":\"ND1\",\"level\":1,\"card\":1}],\"equipSlotIds\":[-1,-1,-1,-1,-1,-1],\"equipInventory\":[{\"id\":0,\"code\":\"SD1\",\"level\":1,\"exp\":0,\"locked\":false,\"option\":[1],\"optionGrade\":[1],\"optionLocked\":[0,0,0,0],\"elementIndex\":0},{\"id\":1,\"code\":\"SD1\",\"level\":1,\"exp\":0,\"locked\":false,\"option\":[0],\"optionGrade\":[0],\"optionLocked\":[0,0,0,0],\"elementIndex\":4},{\"id\":2,\"code\":\"ND1\",\"level\":1,\"exp\":0,\"locked\":false,\"option\":[2],\"optionGrade\":[1],\"optionLocked\":[0,0,0,0],\"elementIndex\":0},{\"id\":3,\"code\":\"RD1\",\"level\":1,\"exp\":0,\"locked\":false,\"option\":[2],\"optionGrade\":[1],\"optionLocked\":[0,0,0,0],\"elementIndex\":2},{\"id\":4,\"code\":\"CD1\",\"level\":1,\"exp\":0,\"locked\":false,\"option\":[7],\"optionGrade\":[2],\"optionLocked\":[0,0,0,0],\"elementIndex\":3}],\"timezoneOffset\":-540,\"debugTimeAdd\":0,\"attendanceIndex\":5,\"attendanceGot\":0,\"attendanceFirst\":1,\"oneSignalId\":\"07b9ecea-4be7-477b-bf30-cdaa3e48025a\",\"equipSlotUnlock\":[1,1,1,1,1,1],\"shopEquipGachaUnlock\":[1,1,1,1,1,1],\"labyrinthLantern\":0,\"enhanceStone\":0,\"enhanceStoneLesser\":0,\"enhanceStoneGreater\":0,\"labyrinthDantalionLevel\":1,\"labyrinthDantalionLevelSelected\":1,\"labyrinthEligosLevel\":1,\"labyrinthEligosLevelSelected\":1,\"labyrinthPaimonLevel\":1,\"labyrinthPaimonLevelSelected\":1,\"passAdsHunt\":2,\"passAdsFreeRecieved\":[],\"passAdsPaidRecieved\":[],\"passStageFreeRecieved\":[],\"passStagePaidRecieved\":[],\"passLevelFreeRecieved\":[],\"passLevelPaidRecieved\":[],\"passStageProduct\":0,\"passLevelProduct\":1,\"lantern\":0}\"},\"__time__\":\"2022-03-07 20:00:09\"}";

    }

    

    public void showAdTestButtonClick()
    {
        var request = new LoginWithCustomIDRequest { CustomId = $"Android.g07083369896476894847", CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);


        //// Google Login
        //PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptAlways, (result) => {
        //    // handle results
        //    Debug.Log("Authenticate()");
        //    Debug.Log(result.ToString());
        //    Debug.Log($"PlayGamesPlatform.Instance.localUser.id {PlayGamesPlatform.Instance.localUser.id}");
        //    //2022 / 03 / 16 10:09:18.564 13724 13800 Info Unity PlayGamesPlatform.Instance.localUser.id g07083369896476894847


        //    var request = new LoginWithCustomIDRequest { CustomId = $"Android.{PlayGamesPlatform.Instance.localUser.id}", CreateAccount = true };
        //    PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
        //});


        //Debug.Log("showAdTestButtonClick()");
        //if (this.rewardedAd.IsLoaded())
        //{
        //    Debug.Log("rewardedAd.IsLoaded()");
        //    this.rewardedAd.Show();
        //}
        //else
        //{
        //    Debug.Log("rewardedAd.IsLoaded() not");
        //}
    }

    private void OnLoginSuccess(LoginResult loginResult)
    {
        Debug.Log("Congratulations, you made your first successful API call!");
        Debug.Log(loginResult.PlayFabId);
        //2022 / 03 / 16 10:09:20.056 13724 13800 Info Unity BFEC56F95A6F13F5
        

        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = loginResult.PlayFabId,
            Keys = null,//{ "$player" },
        }, result => {
            Debug.Log("Got user data:");
            Debug.Log(result.Data.ToString());
            //if (result.Data == null || !result.Data.ContainsKey("Ancestor")) Debug.Log("No Ancestor");
            //else Debug.Log("Ancestor: " + result.Data["Ancestor"].Value);
            foreach (var eachData in result.Data)
            {
                Debug.Log($"{eachData.Key} {eachData.Value.Value}");
            }
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport());
        });
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call.  :(");
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }


    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.LoadAdError.GetMessage());
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        Debug.Log(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.AdError.GetMessage());
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardedAdClosed event received");
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        Debug.Log(
            "HandleRewardedAdRewarded event received for "
                        + amount.ToString() + " " + type);
    }

    // Update is called once per frame
    //void Update()
    //{

    //}
}
